// Bài 1.
/*
  Input: Nhập số 3 số nguyên 

  Các bước thức hiện:
  - Kiểm tra dữ liệu nhập vào. Sai thì nhập lại
  - Nếu đúng thì làm các bước sau
  - Tạo biến inputNum1El, inputNum2El, inputNum3El, tmp
  - Cho người dùng nhập và gán vào inputNum1El
  - Cho người dùng nhập và gán vào inputNum2El
  - Cho người dùng nhập và gán vào inputNum3El
  - So sánh từng cặp (inputNum1El, inputNum2El), (inputNum1El, inputNum3El), (inputNum2El, inputNum3El)
    + Nếu inputNum1El < inputNum2El sai thì inputNum1El >= inputNum2El
    + Nếu inputNum1El < inputNum3El sai thì inputNum1El >= inputNum3El. Kết hợp trường hợp trên là thấy inputNum1El lớn nhất
    + Nếu inputNum2El < inputNum3El sai thì inputNum2El>= inputNum3El. Tương ứng với inputNum3El nhỏ nhất
  - Xuất kết quả
  
  Output: Hiển thì 3 nguyên theo thứ tự tăng dần


*/

const btnSortNumber = document.getElementById("sortNumber");

btnSortNumber.addEventListener("click", function () {
  var a = document.forms["formSortNumber"]["inputNum1"].value;
  var b = document.forms["formSortNumber"]["inputNum2"].value;
  var c = document.forms["formSortNumber"]["inputNum3"].value;
  if (isNaN(a) || isNaN(b) || isNaN(c)) {
    alert("Dữ liệu không hợp lệ");
    return false;
  } else {
    var inputNum1El = document.getElementById("inputNum1").value * 1;
    var inputNum2El = document.getElementById("inputNum2").value * 1;
    var inputNum3El = document.getElementById("inputNum3").value * 1;
    var tmp;
    if (inputNum1El < inputNum2El) {
      tmp = inputNum1El;
      inputNum1El = inputNum2El;
      inputNum2El = tmp;
    }
    if (inputNum1El < inputNum3El) {
      tmp = inputNum1El;
      inputNum1El = inputNum3El;
      inputNum3El = tmp;
    }
    if (inputNum2El < inputNum3El) {
      tmp = inputNum2El;
      inputNum2El = inputNum3El;
      inputNum3El = tmp;
    }
    document.getElementById(
      "txtSortNumber"
    ).innerHTML = `${inputNum3El}, ${inputNum2El}, ${inputNum1El}`;
  }
});

// Bài 2
/*
  Input: Chọn thành viên

  Các bước thức hiện:
  - Tạo biến selUserEl
  - Cho người dùng chọn và gán vào selUserEl
  - Đối chiếu với giá trị chọn sẽ tương ứng với lời chào như sau:
     + Nếu chọm giá trị B xuất ra câu chào với Bố;  
     + Nếu chọm giá trị M xuất ra câu chào với Mẹ; 
     + Nếu chọm giá trị A xuất ra câu chào với Anh trai; 
     + Nếu chọm giá trị E xuất ra câu chào với Em gáo; 
     + Nếu khác tất cả các giá trị trên xuất ra câu chào với Người lạ ơi;
  
     Output: Xuất câu chào với thành viên tương ứng
*/

const btnHelloEl = document.getElementById("btnHello");

btnHelloEl.addEventListener("click", function () {
  var selUserEl = document.getElementById("selUser").value;
  switch (selUserEl) {
    case "B":
      document.getElementById("txtHello").innerHTML = `Xin chào ${selUserEl}`;
      break;
    case "M":
      document.getElementById("txtHello").innerHTML = `Xin chào Mẹ`;
      break;
    case "A":
      document.getElementById("txtHello").innerHTML = `Xin chào Anh Trai`;
      break;
    case "E":
      document.getElementById("txtHello").innerHTML = `Xin chào Em Gái`;
      break;
    default:
      document.getElementById("txtHello").innerHTML = `Xin chào Người lạ ơi`;
      break;
  }
});

// Bài 3
/*
  Input: Nhập số 3 số nguyên 

  Các bước thức hiện:
  - Kiểm tra dữ liệu nhập vào. Sai thì nhập lại
  - Nếu đúng thì làm các bước sau
  - Tạo biến inputCount1El, inputCount2El, inputCount3El, count = 0;
  - Cho người dùng nhập và gán vào inputCount1El
  - Cho người dùng nhập và gán vào inputCount2El
  - Cho người dùng nhập và gán vào inputCount3El
  - Dùng phép chia lấy dư với 2 để xác định số lẻ
    + Nếu inputCount1El % 2 == 1 thì biến count cộng thêm 1
    + Nếu inputCount2El % 2 == 1 thì biến count cộng thêm 1
    + Nếu inputCount3El % 2 == 1 thì biến count cộng thêm 1
  - Xuất kết quả
     Output: Đếm số chẵn số lẻ
*/

const btnCountEl = document.getElementById("btnCount");

btnCountEl.addEventListener("click", function () {
  var a = document.forms["formCount"]["inputCount1"].value;
  var b = document.forms["formCount"]["inputCount2"].value;
  var c = document.forms["formCount"]["inputCount3"].value;
  if (isNaN(a) || isNaN(b) || isNaN(c)) {
    alert("Dữ liệu không hợp lệ");
    return false;
  } else {
    var inputCount1El = document.getElementById("inputCount1").value * 1;
    var inputCount2El = document.getElementById("inputCount2").value * 1;
    var inputCount3El = document.getElementById("inputCount3").value * 1;
    var count = 0;
    if (inputCount1El % 2 == 1) count += 1;
    if (inputCount2El % 2 == 1) count += 1;
    if (inputCount3El % 2 == 1) count += 1;
    document.getElementById("txtCount").innerHTML = `Có ${
      3 - count
    } số chẵn, ${count} số lẻ`;
  }
});

// Bài 3
/*
  Input: Nhập số 3 cạnh  

  Các bước thức hiện:
  - Kiểm tra dữ liệu nhập vào. Sai thì nhập lại
  - Nếu đúng thì làm các bước sau
  - Tạo biến inputEdge1El, inputEdge2El, inputEdge3El;
  - Cho người dùng nhập và gán vào inputEdge1El
  - Cho người dùng nhập và gán vào inputEdge2El
  - Cho người dùng nhập và gán vào inputCount3El
  - Kiểm tra 3 cạnh có phải 3 cạnh của một tam giác không
    Nếu 3 cạnh trên làm 3 cạnh của một tam giác. Xét đếm các trường hợp
    + Nếu tổng bình phương 2 cạnh của tam giác bằng tổng bình bình cạnh còn lại thì tam giác vuông
    + Nếu 3 cạnh bằng nhau là tam giác đều
    + Nếu 2 cạnh bằng nhau là tam giác cân
    + Còn lại là tam giác khác
  - Xuất kết quả
     Output: Xuất ra loại tam giác
*/

const btnEdgeEl = document.getElementById("btnEdge");

btnEdgeEl.addEventListener("click", function () {
  var a = document.forms["formEdge"]["inputEdge1"].value;
  var b = document.forms["formEdge"]["inputEdge2"].value;
  var c = document.forms["formEdge"]["inputEdge3"].value;
  if (isNaN(a) || isNaN(b) || isNaN(c)) {
    alert("Dữ liệu không hợp lệ");
    return false;
  } else {
    var inputEdge1El = document.getElementById("inputEdge1").value * 1;
    var inputEdge2El = document.getElementById("inputEdge2").value * 1;
    var inputEdge3El = document.getElementById("inputEdge3").value * 1;
    if (
      inputEdge1El < inputEdge2El + inputEdge3El &&
      inputEdge2El < inputEdge1El + inputEdge3El &&
      inputEdge3El < inputEdge1El + inputEdge2El
    ) {
      if (
        inputEdge1El * inputEdge1El ==
          inputEdge2El * inputEdge2El + inputEdge3El * inputEdge3El ||
        inputEdge2El * inputEdge2El ==
          inputEdge1El * inputEdge1El + inputEdge3El * inputEdge3El ||
        inputEdge3El * inputEdge3El ==
          inputEdge1El * inputEdge1El + inputEdge2El * inputEdge2El
      ) {
        document.getElementById("txtEdge").innerHTML = `Tam giác vuông`;
      } else if (inputEdge1El == inputEdge2El && inputEdge2El == inputEdge3El) {
        document.getElementById("txtEdge").innerHTML = `Tam giác đều`;
      } else if (
        inputEdge1El == inputEdge2El ||
        inputEdge1El == inputEdge3El ||
        inputEdge2El == inputEdge3El
      ) {
        document.getElementById("txtEdge").innerHTML = `Tam giác cân`;
      } else {
        document.getElementById("txtEdge").innerHTML = `Một loại tam giác khác`;
      }
    } else {
      alert("Đây không phải là 3 cạnh của 1 tam giác");
    }
  }
});
